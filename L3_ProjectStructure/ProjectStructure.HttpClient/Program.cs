﻿using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Show;

namespace ProjectStructure.Program
{
    class Program
    {

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter command(? - to call /help):");
                Console.ForegroundColor = ConsoleColor.White;

                string input = Console.ReadLine();
                try
                {
                    Console.WriteLine();
                    switch (input)
                    {
                        case "1":
                            await ShowUI.GetCountOfTasksOfProjectsByUserId(ShowUI.GetIdFromUser());
                            break;
                        case "2":
                            await ShowUI.GetTasksByUserIdWithShortName(ShowUI.GetIdFromUser());
                            break;
                        case "3":
                            await ShowUI.GetFinishedTaskByUserIdInThisYear(ShowUI.GetIdFromUser());
                            break;
                        case "4":
                            await ShowUI.GetTeamsWithUsersOlderThan9(ShowUI.GetIdFromUser());
                            break;
                        case "5":
                            await ShowUI.GetOrderedUsersAndTasks();
                            break;
                        case "6":
                            await ShowUI.GetInfoAboutTasksByUserId(ShowUI.GetIdFromUser());
                            break;
                        case "7":
                            await ShowUI.GetInfoAboutProjects();
                            break;
                        case "8":
                            await ShowUI.GetProjects();
                            break;
                        case "9":
                            await ShowUI.GetTasks();
                            break;
                        case "10":
                            await ShowUI.GetTeams();
                            break;
                        case "11":
                            await ShowUI.GetUsers();
                            break;
                        case "exit":
                            return;
                        case "?":
                            ShowUI.GetCommandMenu();
                            break;
                    }
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n");
                }
            }
        }
    }
}
