﻿using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Context _context;
        private IProjectRepository _projectRepository;
        private ITaskRepository _taskRepository;
        private ITeamRepository _teamRepository;
        private IUserRepository _userRepository;

        public UnitOfWork(Context context)
        {
            _context = context;
        }

        public IProjectRepository GetProjectRepository
        {
            get
            {
                return _projectRepository = _projectRepository
                    ?? new ProjectRepository(_context, _context.Projects);
            }
        }

        public ITaskRepository GetTaskRepository
        {
            get
            {
                return _taskRepository = _taskRepository
                    ?? new TaskRepository(_context, _context.Tasks);
            }
        }

        public ITeamRepository GetTeamRepository
        {
            get
            {
                return _teamRepository = _teamRepository
                    ?? new TeamRepository(_context, _context.Teams);
            }
        }

        public IUserRepository GetUserRepository
        {
            get
            {
                return _userRepository = _userRepository
                    ?? new UserRepository(_context, _context.Users);
            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }
    }
}
