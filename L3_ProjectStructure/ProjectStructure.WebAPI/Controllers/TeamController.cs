﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        private readonly IMapper _mapper;

        public TeamController(ITeamService teamService, IMapper mapper)
        {
            _teamService = teamService;
            _mapper = mapper;
        }

        // GET: api/<TeamController>
        [HttpGet]
        public ActionResult<IList<GettingTeamView>> Get()
        {
            var teamsDTO = _teamService.GetAll();
            var teamsView = _mapper.Map<IList<TeamDTO>, IList<GettingTeamView>>(teamsDTO);

            return Ok(teamsView);
        }


        // POST api/<TeamController>
        [HttpPost]
        public ActionResult Post([FromBody] TeamView teamView)
        {
            try
            {
                var teamDTO = _mapper.Map<TeamView, TeamDTO>(teamView);

                _teamService.Create(teamDTO);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        // PUT api/<TeamController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TeamView teamView)
        {
            try
            {
                var teamDTO = _mapper.Map<TeamView, TeamDTO>(teamView);
                teamDTO.Id = id;
                _teamService.Update(teamDTO);

                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // DELETE api/<TeamController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _teamService.Delete(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/<TeamController>
        [HttpGet("q5")]
        public ActionResult<IList<Query4DTO>> GetTeamsWithUsersOlderThan9()
        {
            try
            {
                var result = _teamService.GetTeamsWithUsersOlderThan9();

                return Ok(result);
            }
            catch(Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}
