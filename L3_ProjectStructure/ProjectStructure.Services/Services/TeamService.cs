﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private static int idIncrementor;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            idIncrementor = 0;
        }

        public void Create(TeamDTO teamDTO)
        {
            if (teamDTO == null)
                throw new Exception();

            var teamEntity = _mapper.Map<Team>(teamDTO);

            teamEntity.Id = idIncrementor;
            idIncrementor++;
            teamEntity.CreatedAt = DateTime.Now;

            _unitOfWork.GetTeamRepository.Create(teamEntity);
        }

        public IList<TeamDTO> GetAll()
        {
            return _mapper.Map<IList<Team>, IList<TeamDTO>>(_unitOfWork.GetTeamRepository.Get());
        }

        public void Update(TeamDTO teamDTO)
        {
            var teamToUpdate = _unitOfWork.GetTeamRepository.Get().FirstOrDefault(team => team.Id == teamDTO.Id);

            if (teamToUpdate == null)
                throw new Exception();

            _unitOfWork.GetTeamRepository.Update(_mapper.Map<Team>(teamDTO));
        }

        public void Delete(int id)
        {
            var teamToDelete = _unitOfWork.GetTeamRepository.Get().FirstOrDefault(team => team.Id == id);

            if (teamToDelete == null)
                throw new Exception();

            _unitOfWork.GetTeamRepository.Delete(id);
        }

        public IList<Query4DTO> GetTeamsWithUsersOlderThan9()
        {
            var teams = _unitOfWork.GetTeamRepository.Get();
            var users = _unitOfWork.GetUserRepository.Get();

            var result = teams.GroupJoin(users,
                team => team.Id,
                user => user.TeamId,
                (team, users) =>
                (Id: team.Id, Name: team.Name, Members: users.OrderByDescending(user => user.RegisteredAt)))
                .Where(team => team.Members.All(member => DateTime.Now.Year - member.BirthDay.Year > 10)).ToList();

            if (!result.Any())
                throw new ArgumentException("There are no such teams");

            var resultQuery = result.Select(query4DTO => new Query4DTO
            {
                Id = query4DTO.Id,
                Name = query4DTO.Name,
                Members = _mapper.Map<IList<User>, IList<UserDTO>>(query4DTO.Members.ToList())
            }).ToList();

            return resultQuery;
        }
    }
}
