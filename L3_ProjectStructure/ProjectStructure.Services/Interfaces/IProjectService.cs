﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface IProjectService
    {
        void Create(ProjectDTO projectDTO);
        IList<ProjectDTO> GetAll();
        void Update(ProjectDTO projectDTO);
        void Delete(int id);

        IList<Query1DTO> GetCountOfTasksOfProjectsByUserId(int id);
        IList<Query7DTO> GetInfoAboutProjects();
    }
}
