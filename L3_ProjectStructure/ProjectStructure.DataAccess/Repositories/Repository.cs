﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly Context _context;
        protected readonly IList<TEntity> _entities;

        public Repository(Context context, IList<TEntity> entities)
        {
            _context = context;
            _entities = entities;
        }
        
        public virtual void Create(TEntity entity) => _entities.Add(entity);

        public virtual void Delete(int id) {
            var entityToDelete = _entities.FirstOrDefault(entity => entity.Id == id);
            _entities.Remove(entityToDelete);
        }

        public virtual IList<TEntity> Get(Func<TEntity, bool> filter = null) => _entities?.ToList();

        public virtual void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
