﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(Context context, IList<User> users)
            : base(context, users)
        {
        }

        public override void Update(User user)
        {
            var userToUpdate = _entities.FirstOrDefault(e => e.Id == user.Id);

            userToUpdate.TeamId = user.TeamId;
            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            userToUpdate.Email = user.Email;
            userToUpdate.BirthDay = user.BirthDay;
        }
    }
}
