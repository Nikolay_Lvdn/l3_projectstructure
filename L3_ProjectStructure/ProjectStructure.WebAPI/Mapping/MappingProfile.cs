﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.WebAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Task, TaskDTO>().ReverseMap();
            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<ProjectDTO, GettingProjectView>().ReverseMap();
            CreateMap<ProjectDTO, ProjectView>().ReverseMap();

            CreateMap<TaskDTO, GettingTaskView>().ReverseMap();
            CreateMap<TaskDTO, TaskView>().ReverseMap();

            CreateMap<TeamDTO, GettingTeamView>().ReverseMap();
            CreateMap<TeamDTO, TeamView>().ReverseMap();

            CreateMap<UserDTO, GettingUserView>().ReverseMap();
            CreateMap<UserDTO, UserView>().ReverseMap();
        }
    }
}
