﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(Context context, IList<Team> teams)
            : base(context, teams)
        {
        }

        public override void Update(Team team)
        {
            var teamToUpdate = _entities.FirstOrDefault(e => e.Id == team.Id);

            teamToUpdate.Name = team.Name;
        }
    }
}
