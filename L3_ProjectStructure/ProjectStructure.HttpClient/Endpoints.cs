﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.WebAPI.Models;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;

namespace ProjectStructure.ServiceCommunication
{
    public static class Endpoints
    {
        private static HttpClient client = new HttpClient();

        public static async Task<IList<Query1DTO>> GetCountOfTasksOfProjectsByUserId(int id)
        {
            var url = "https://localhost:44310/api/Project/q1/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query1DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<TaskDTO>> GetTasksByUserIdWithShortName(int id)
        {
            var url = "https://localhost:44310/api/Task/q2/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<TaskDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query3DTO>> GetFinishedTaskByUserIdInThisYear(int id)
        {
            var url = "https://localhost:44310/api/Task/q3/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query3DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query4DTO>> GetTeamsWithUsersOlderThan9(int id)
        {
            var url = "https://localhost:44310/api/Team/q4/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query4DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query5DTO>> GetOrderedUsersAndTasks()
        {
            var url = "https://localhost:44310/api/User/q5";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query5DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query6DTO>> GetInfoAboutTasksByUserId(int id)
        {
            var url = "https://localhost:44310/api/User/q6/?id=" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query6DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<Query7DTO>> GetInfoAboutProjects()
        {
            var url = "https://localhost:44310/api/Project/q7";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IList<Query7DTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<GettingProjectView>> GetProjects()
        {
            var url = "https://localhost:44310/api/Project";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<GettingProjectView>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<IList<GettingTaskView>> GetTasks()
        {
            var url = "https://localhost:44310/api/Task";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<GettingTaskView>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<IList<GettingTeamView>> GetTeams()
        {
            var url = "https://localhost:44310/api/Team";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<GettingTeamView>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<IList<GettingUserView>> GetUsers()
        {
            var url = "https://localhost:44310/api/User";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<GettingUserView>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task GetResponceMessage(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.NotFound)
                Console.WriteLine(await response.Content.ReadAsStringAsync());
        }
    }
}
