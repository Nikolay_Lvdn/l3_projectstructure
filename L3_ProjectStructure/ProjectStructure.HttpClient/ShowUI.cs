﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;
using ProjectStructure.ServiceCommunication;
using ProjectStructure.WebAPI.Models;

namespace ProjectStructure.Show
{
    public static class ShowUI
    {
        public static async Task GetCountOfTasksOfProjectsByUserId(int id)
        {
            List<Query1DTO> result = (List<Query1DTO>)await Endpoints.GetCountOfTasksOfProjectsByUserId(id);

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetTasksByUserIdWithShortName(int id)
        {
            List<TaskDTO> result = (List<TaskDTO>)await Endpoints.GetTasksByUserIdWithShortName(id);

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetFinishedTaskByUserIdInThisYear(int id)
        {
            List<Query3DTO> result = (List<Query3DTO>)await Endpoints.GetFinishedTaskByUserIdInThisYear(id);

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetTeamsWithUsersOlderThan9(int id)
        {
            List<Query4DTO> result = (List<Query4DTO>)await Endpoints.GetTeamsWithUsersOlderThan9(id);

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetOrderedUsersAndTasks()
        {
            List<Query5DTO> result = (List<Query5DTO>)await Endpoints.GetOrderedUsersAndTasks();

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetInfoAboutTasksByUserId(int id)
        {
            List<Query6DTO> result = (List<Query6DTO>)await Endpoints.GetInfoAboutTasksByUserId(id);

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetInfoAboutProjects()
        {
            List<Query7DTO> result = (List<Query7DTO>)await Endpoints.GetInfoAboutProjects();

            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public static async Task GetProjects()
        {
            List<GettingProjectView> projects = (List<GettingProjectView>)await Endpoints.GetProjects();

            Console.WriteLine(JsonConvert.SerializeObject(projects));
        }

        public static async Task GetTasks()
        {
            List<GettingTaskView> projects = (List<GettingTaskView>)await Endpoints.GetTasks();

            Console.WriteLine(JsonConvert.SerializeObject(projects));
        }

        public static async Task GetTeams()
        {
            List<GettingTeamView> projects = (List<GettingTeamView>)await Endpoints.GetTeams();

            Console.WriteLine(JsonConvert.SerializeObject(projects));
        }

        public static async Task GetUsers()
        {
            List<GettingUserView> projects = (List<GettingUserView>)await Endpoints.GetUsers();

            Console.WriteLine(JsonConvert.SerializeObject(projects));
        }

        public static void GetCommandMenu()
        {
            Console.WriteLine("1 - query1");
            Console.WriteLine("2 - query2");
            Console.WriteLine("3 - query3");
            Console.WriteLine("4 - query4");
            Console.WriteLine("5 - query5");
            Console.WriteLine("6 - query6");
            Console.WriteLine("7 - query7");
            Console.WriteLine("8 - Display all projects.");
            Console.WriteLine("9 - Display all Tasks.");
            Console.WriteLine("10 - Display all Teams.");
            Console.WriteLine("11 - Display all Users.");
            Console.WriteLine("exit - to exit the program");
        }

        public static int GetIdFromUser()
        {
            int id;
            Console.WriteLine("Enter Id");
            id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            return id;
        }
    }
}
