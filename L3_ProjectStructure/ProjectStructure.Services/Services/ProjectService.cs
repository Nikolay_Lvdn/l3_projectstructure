﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private static int idIncrementor;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            idIncrementor = 0;
        }

        public void Create(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                throw new Exception();

            var projectEntity = _mapper.Map<Project>(projectDTO);

            projectEntity.Id = idIncrementor;
            idIncrementor++;
            projectEntity.CreatedAt = DateTime.Now;

            _unitOfWork.GetProjectRepository.Create(projectEntity);
        }

        public IList<ProjectDTO> GetAll()
        {
            return _mapper.Map<IList<Project>, IList<ProjectDTO>>(_unitOfWork.GetProjectRepository.Get());
        }

        public void Update(ProjectDTO projectDTO)
        {
            var projectToUpdate = _unitOfWork.GetProjectRepository.Get().FirstOrDefault(project => project.Id == projectDTO.Id);

            if (projectToUpdate == null)
                throw new Exception();

            _unitOfWork.GetProjectRepository.Update(_mapper.Map<Project>(projectDTO));
        }

        public void Delete(int id)
        {
            var projectToDelete = _unitOfWork.GetProjectRepository.Get().FirstOrDefault(project => project.Id == id);

            if (projectToDelete == null)
                throw new Exception();

            _unitOfWork.GetProjectRepository.Delete(id);
        }

        public IList<Query1DTO> GetCountOfTasksOfProjectsByUserId(int id)
        {
            var projects = _unitOfWork.GetProjectRepository.Get();
            var tasks = _unitOfWork.GetTaskRepository.Get();

            var result = projects.Where(project => project.AuthorId == id)
                .ToDictionary(project => project, project => tasks.Count(task => task.ProjectId == project.Id));

            var resultQuery = result.Select(query1DTO => new Query1DTO
            {
                Project = _mapper.Map<ProjectDTO>(query1DTO.Key),
                Count = query1DTO.Value
            }).ToList();

            if (!resultQuery.Any())
                throw new ArgumentException("User not author of any project");

            return resultQuery;
        }

        public IList<Query7DTO> GetInfoAboutProjects()
        {
            var tasks = _unitOfWork.GetTaskRepository.Get();
            var projects = _unitOfWork.GetProjectRepository.Get();
            var users = _unitOfWork.GetUserRepository.Get();

            var result = projects.Where(project => project.Description.Length > 20 || tasks.Count(task=>task.ProjectId==project.Id) < 3)
                .GroupJoin(users,
                project => project.TeamId,
                user => user.TeamId,
                (project, users) => (Project: project, Users: users))
                .Select(data => (
                Project: data.Project,
                LongestTask: data.Project.Tasks?.OrderBy(t => t.Description).LastOrDefault(),
                ShortestTask: data.Project.Tasks?.OrderBy(t => t.Name).FirstOrDefault(),
                CountOfMembers: data.Users?.Count()
                ));

            if (!result.Any())
                throw new ArgumentException("There are no projects");

            var resultQuery = result.Select(query7DTO => new Query7DTO
            {
                Project = _mapper.Map<ProjectDTO>(query7DTO.Project),
                LongestTask = _mapper.Map<TaskDTO>(query7DTO.LongestTask),
                ShortestTask = _mapper.Map<TaskDTO>(query7DTO.ShortestTask),
                CountOfMembers = query7DTO.CountOfMembers
            }).ToList();

            return resultQuery;
        }
    }
}
