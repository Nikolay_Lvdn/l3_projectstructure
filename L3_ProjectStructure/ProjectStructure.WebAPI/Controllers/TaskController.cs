﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;

        public TaskController(ITaskService taskService, IMapper mapper)
        {
            _taskService = taskService;
            _mapper = mapper;
        }

        // GET: api/<TaskController>
        [HttpGet]
        public ActionResult<IList<GettingTaskView>> Get()
        {
            var tasksDTO = _taskService.GetAll();
            var tasksView = _mapper.Map<IList<TaskDTO>, IList<GettingTaskView>>(tasksDTO);

            return Ok(tasksView);
        }

        // POST api/<TaskController>
        [HttpPost]
        public ActionResult Post([FromBody] TaskView taskView)
        {
            try
            {
                var taskDTO = _mapper.Map<TaskView, TaskDTO>(taskView);

                _taskService.Create(taskDTO);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TaskView taskView)
        {
            try
            {
                var taskDTO = _mapper.Map<TaskView, TaskDTO>(taskView);
                taskDTO.Id = id;
                _taskService.Update(taskDTO);

                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskService.Delete(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/<TaskController>/q2
        [HttpGet("q2")]
        public ActionResult<IList<TaskDTO>> GetTasksByUserIdWithShortName(int id)
        {
            try
            {
                var result = _taskService.GetTasksByUserIdWithShortName(id);

                return Ok(result);
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/<TaskController>/q3
        [HttpGet("q3")]
        public ActionResult<IList<Query3DTO>> GetFinishedTaskByUserIdInThisYear(int id)
        {
            try
            {
                var result = _taskService.GetFinishedTaskByUserIdInThisYear(id);

                return Ok(result);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
