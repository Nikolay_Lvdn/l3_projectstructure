﻿using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DataAccess
{
    public class Context
    {
        /*public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }*/
        public List<Project> Projects = new List<Project>();
        public List<Task> Tasks = new List<Task>();
        public List<Team> Teams = new List<Team>();
        public List<User> Users = new List<User>();

        /*public Context()
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            Teams = new List<Team>();
            Users = new List<User>();
        }*/
    }
}
