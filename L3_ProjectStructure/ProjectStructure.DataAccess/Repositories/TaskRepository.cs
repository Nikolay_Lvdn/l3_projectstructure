﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DataAccess.Repositories
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        public TaskRepository(Context context, IList<Task> tasks)
            : base(context, tasks)
        {
        }

        public override void Update(Task task)
        {
            var taskToUpdate = _entities.FirstOrDefault(e => e.Id == task.Id);

            taskToUpdate.ProjectId = task.ProjectId;
            taskToUpdate.PerformerId = task.PerformerId;
            taskToUpdate.Name = task.Name;
            taskToUpdate.State = task.State;
            taskToUpdate.FinishedAt = task.FinishedAt;
        }
    }
}
