﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private static int idIncrementor;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            idIncrementor = 0;
        }

        public void Create(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new Exception();

            var taskEntity = _mapper.Map<Task>(taskDTO);

            taskEntity.Id = idIncrementor;
            idIncrementor++;
            taskEntity.CreatedAt = DateTime.Now;

            _unitOfWork.GetTaskRepository.Create(taskEntity);
        }

        public IList<TaskDTO> GetAll()
        {
            return _mapper.Map<IList<Task>, IList<TaskDTO>>(_unitOfWork.GetTaskRepository.Get());
        }

        public void Update(TaskDTO taskDTO)
        {
            var taskToUpdate = _unitOfWork.GetTaskRepository.Get().FirstOrDefault(task => task.Id == taskDTO.Id);

            if (taskToUpdate == null)
                throw new Exception();

            _unitOfWork.GetTaskRepository.Update(_mapper.Map<Task>(taskDTO));
        }

        public void Delete(int id)
        {
            var taskToDelete = _unitOfWork.GetTaskRepository.Get().FirstOrDefault(task => task.Id == id);

            if (taskToDelete == null)
                throw new Exception();

            _unitOfWork.GetTaskRepository.Delete(id);
        }

        public IList<TaskDTO> GetTasksByUserIdWithShortName(int id)
        {
            var tasks = _unitOfWork.GetTaskRepository.Get();

            List<Task> result = tasks.Where(task => task.PerformerId == id && task.Name.Length < 45).ToList();

            if (!result.Any())
                throw new ArgumentException("There are no such tasks");

            return _mapper.Map<IList<Task>, IList<TaskDTO>>(result);
        }

        public IList<Query3DTO> GetFinishedTaskByUserIdInThisYear(int id)
        {
            var tasks = _unitOfWork.GetTaskRepository.Get();

            List<(int, string)> result = tasks.Where(task => task.PerformerId == id
                 && task.FinishedAt != null
                 && task.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select(t => (Id: t.Id, Name: t.Name))
                .ToList();

            if (!result.Any())
                throw new ArgumentException("There are no such tasks");

            var resultQuery = result.Select(query3DTO => new Query3DTO
            {
                Id = query3DTO.Item1,
                Name = query3DTO.Item2
            }).ToList();

            return resultQuery;
        }
    }
}
