﻿using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(Context context, IList<Project> projects)
            : base(context, projects)
        {
        }

        public override void Update(Project project)
        {
            var projectToUpdate = _entities.FirstOrDefault(e => e.Id == project.Id);

            projectToUpdate.AuthorId = project.AuthorId;
            projectToUpdate.TeamId = project.TeamId;
            projectToUpdate.Name = project.Name;
            projectToUpdate.Description = project.Description;
            projectToUpdate.Deadline = project.Deadline;
        }
    }
}
