﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Business.Interfaces;
using AutoMapper;
using ProjectStructure.Business.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        // GET: api/<UserController>
        [HttpGet]
        public ActionResult<IList<GettingUserView>> Get()
        {
            var membersDTO = _userService.GetAll();
            var membersView = _mapper.Map<IList<UserDTO>, IList<GettingUserView>>(membersDTO);

            return Ok(membersView);
        }

        // GET api/<UserController>/5
        /*[HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }*/

        // POST api/<UserController>
        [HttpPost]
        public ActionResult Post([FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);

                _userService.Create(userDTO);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id,[FromBody] UserView userView)
        {
            try
            {
                var userDTO = _mapper.Map<UserView, UserDTO>(userView);
                userDTO.Id = id;
                _userService.Update(userDTO);

                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/<UserController>/q5
        [HttpGet("q5")]
        public ActionResult<IList<Query5DTO>> GetOrderedUsersAndTasks()
        {
            try
            {
                var result = _userService.GetOrderedUsersAndTasks();

                return Ok(result);
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/<UserController>/q6
        [HttpGet("q6")]
        public ActionResult<Query6DTO> GetInfoAboutTasksByUserId(int id)
        {
            try
            {
                var result = _userService.GetInfoAboutTasksByUserId(id);

                return Ok(result);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
