﻿using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
       
        void Create(TEntity entity);
        void Delete(int id);
        IList<TEntity> Get(Func<TEntity, bool> filter = null);
        void Update(TEntity entity);
    }
}
